# Syntax

Syntax is very minimal:

* Quotation markers "[" and "]",
* Line comments starting with "#",
* String literals enclosed by double quotes ('"'),
* Integer and Float literals (e.g. 123 or 123.456),
* and Atoms (everything else until a whitespace).

To escape a double quote within a string literal, use two double quotes.
