//! The Runtime system

use crate::compiler::{
    Instruction, InstructionSeq, PrimitiveId, Program, QuotationId, StringId, SymbolId, Value,
};
use crate::symbol_map::SymbolMap;
use std::collections::HashMap;

// A `Program` only contains a symbol to symbol id mapping, but no assignment between quotations
// anonymous functions), but not the actual definitions The runtime system needs
// (which is "code") and symbol ids, nor the assignment of primitives to symbol ids.

/// A value on the Data stack.
#[derive(Debug, Clone)]
pub enum Data {
    /// Yeah, we support Null. XXX: Define behaviour of Null. XXX: Rename to bottom?
    Null,
    /// Boolean value
    Bool(bool),
    /// Integer value
    Integer(isize),
    /// Float value
    Float(f64),
    /// Reference to a string constant.
    ConstString(StringId),
    /// A dynamically allocated string.
    DynString(String),
    /// A reference to a constant (i.e. not dynamically allocated) quotation.
    /// XXX: If we need to, we could have dynamic quotation  as well...
    ConstQuotation(QuotationId),
}

// Alternatively, we could use a single usize to point into linear memory, but
// this would mean that we have to flatten and layout all quotations linear in
// memory, and introduce RET operations. This is probably a good idea, but for
// now take the easy way (so we can reuse the compiler::Instruction).
// The problem with the current approach is, that the IP can point beyond an instruction sequence,
// which is not nice. We treat it as a RETURN if it points to the element at [len()] (i.e. one
// beyond). If it points farther outside, this indicates an error.
#[derive(Debug, Copy, Clone)]
struct InstructionPointer {
    /// In which quotation are we currently executing?
    quotation_id: QuotationId,
    /// Offset to the current instruction in the quotation.
    instruction_offset: usize,
}

struct Activation {
    ip: InstructionPointer,
    local_variables: LocalVariables,
}

/// Allows to define local variables overriding global definitions  
#[derive(Debug)]
struct LocalVariables {
    variables: Vec<(SymbolId, Data)>,
}

impl LocalVariables {
    fn new() -> Self {
        Self {
            variables: Vec::new(),
        }
    }
    fn lookup(&self, symbol_id: SymbolId) -> Option<Data> {
        self.variables
            .iter()
            .find(|t| t.0 == symbol_id)
            .map(|t| t.1.clone())
    }
    fn set(&mut self, symbol_id: SymbolId, value: Data) {
        for t in self.variables.iter_mut() {
            if t.0 == symbol_id {
                t.1 = value;
                return;
            }
        }
        self.variables.push((symbol_id, value));
    }
}

#[derive(Debug, Copy, Clone)]
enum DefinitionEntry {
    Quotation(QuotationId),
    Primitive(PrimitiveId),
}

/// GlobalQuotationDictionary: looks up symbol => QuotationId
/// This is dynamic. i.e. we have primitives that can add values to the dictionary.
///
/// In some rare cases, we want to overwrite the global dictionary locally for a quotation that we
/// invoke, i.e. to define local variables (with dynamic scope).  
///
/// Also, if we want to invoke functions dynamically, i.e. without using a compiled symbolid,
/// we need to look them up by name.
/// Looking up by name: We can always create a new symbol for that name in the symbol table, and
/// as such convert it to a symbolId. and then invoke that symbol id. Local variables usually are
/// already in the symbol table available, so we just need to override.

struct GlobalDefinitions {
    /// definitions are indexed by symbol_id. `static_definitions` only contain definitions
    /// for those symbols that were known at compile time. For any other symbol, we insert
    /// a definition into `dynamic_definitions`.
    static_definitions: Box<[Option<DefinitionEntry>]>,
    dynamic_definitions: HashMap<SymbolId, DefinitionEntry>,
}

impl GlobalDefinitions {
    fn register(&mut self, symbol_id: SymbolId, definition_entry: DefinitionEntry) {
        if symbol_id.0 < self.static_definitions.len() {
            self.static_definitions[symbol_id.0] = Some(definition_entry);
        } else {
            self.dynamic_definitions.insert(symbol_id, definition_entry);
        }
    }

    fn lookup(&self, symbol_id: SymbolId) -> Option<DefinitionEntry> {
        if symbol_id.0 < self.static_definitions.len() {
            self.static_definitions[symbol_id.0]
        } else {
            self.dynamic_definitions.get(&symbol_id).cloned()
        }
    }
}

pub struct Rts {
    data_stack: Vec<Data>,
    return_stack: Vec<Activation>,
    current_execution: Activation,

    /// Contains the constant string literals
    string_constants: Vec<String>,

    /// Quotations are anonymous functions. We refer to each using a QuotationId.
    quotations: Vec<InstructionSeq>,

    global_definitions: GlobalDefinitions,

    /// Contains all defined symbols. Can be extended at runtime, i.e. when a string
    /// is converted into a symbol.
    symbols: SymbolMap,
}

#[derive(Debug, Copy, Clone)]
pub enum ReturnFromRts {
    ProgramExited,
    DefinitionMissing(SymbolId),
    PrimitiveCalled(PrimitiveId),
}

impl Rts {
    pub fn pop_data_stack(&mut self) -> Data {
        self.data_stack.pop().unwrap()
    }

    pub fn data_stack(&self) -> &[Data] {
        &self.data_stack[..]
    }

    pub fn push_data_stack(&mut self, data: Data) {
        self.data_stack.push(data);
    }

    /// This will just load the program, but it will not yet load the RTS, i.e.
    /// no primitives are defined yet.
    pub fn from_program(program: Program) -> Self {
        let num_static_symbols = program.symbols.len();
        Self {
            data_stack: Vec::new(),
            return_stack: Vec::new(),
            current_execution: Activation {
                ip: InstructionPointer {
                    quotation_id: program.entry_point,
                    instruction_offset: 0,
                },
                local_variables: LocalVariables::new(),
            },
            string_constants: program.string_constants,
            quotations: program.quotations,
            symbols: program.symbols,
            global_definitions: GlobalDefinitions {
                static_definitions: (0..num_static_symbols)
                    .into_iter()
                    .map(|_| None)
                    .collect::<Vec<_>>()
                    .into_boxed_slice(),
                dynamic_definitions: HashMap::new(),
            },
        }
    }

    fn add_quotation(&mut self, instruction_seq: InstructionSeq) -> QuotationId {
        let id = self.quotations.len();
        self.quotations.push(instruction_seq);
        return QuotationId(id);
    }

    pub fn lookup_string_id(&self, string_id: StringId) -> &str {
        &self.string_constants[string_id.0]
    }

    pub fn symbol_id_for_string_id(&mut self, string_id: StringId) -> SymbolId {
        SymbolId(self.symbols.insert(&self.string_constants[string_id.0]))
    }

    pub fn symbol_id_to_string(&self, symbol_id: SymbolId) -> &str {
        self.symbols.index(symbol_id.0)
    }

    pub fn register_global_primitive(&mut self, symbol_id: SymbolId, primitive_id: PrimitiveId) {
        self.global_definitions
            .register(symbol_id, DefinitionEntry::Primitive(primitive_id));
    }

    pub fn register_global_primitive_from_str(&mut self, symbol: &str, primitive_id: PrimitiveId) {
        let symbol_id = SymbolId(self.symbols.insert(symbol));
        self.register_global_primitive(symbol_id, primitive_id);
    }

    pub fn register_global_quotation(&mut self, symbol_id: SymbolId, quotation_id: QuotationId) {
        self.global_definitions
            .register(symbol_id, DefinitionEntry::Quotation(quotation_id));
    }

    fn return_to_caller(&mut self) -> Result<(), ReturnFromRts> {
        match self.return_stack.pop() {
            Some(activation) => {
                self.current_execution = activation;
                Ok(())
            }
            None => Err(ReturnFromRts::ProgramExited),
        }
    }

    pub fn restart_current_quotation(&mut self) {
        self.current_execution.ip.instruction_offset = 0;
    }

    pub fn activate_quotation(&mut self, quotation_id: QuotationId) {
        let mut current_execution = Activation {
            ip: InstructionPointer {
                quotation_id,
                instruction_offset: 0,
            },
            local_variables: LocalVariables::new(),
        };
        std::mem::swap(&mut current_execution, &mut self.current_execution);
        self.return_stack.push(current_execution);
    }

    // defining and setting are two different things. defining determines the scope.
    pub fn define_local_variable(&mut self, symbol_id: SymbolId) {
        // XXX: Problem is that primitives are called in a quotation instead of directly.
        self.current_execution
            .local_variables
            .set(symbol_id, Data::Null);
    }

    pub fn set_local_variable(&mut self, symbol_id: SymbolId, value: Data) {
        if let Some(_) = self.current_execution.local_variables.lookup(symbol_id) {
            self.current_execution.local_variables.set(symbol_id, value);
            return;
        }
        // recurse stack activations
        for activation in self.return_stack.iter_mut().rev() {
            if let Some(_) = activation.local_variables.lookup(symbol_id) {
                activation.local_variables.set(symbol_id, value);
                return;
            }
        }
        panic!("variable not defined");
    }

    pub fn call_symbol(&mut self, symbol_id: SymbolId) -> Result<(), ReturnFromRts> {
        if let Some(data) = self.current_execution.local_variables.lookup(symbol_id) {
            self.data_stack.push(data);
            return Ok(());
        }
        // recurse stack activations
        for activation in self.return_stack.iter().rev() {
            if let Some(data) = activation.local_variables.lookup(symbol_id) {
                self.data_stack.push(data);
                return Ok(());
            }
        }

        match self.global_definitions.lookup(symbol_id) {
            Some(DefinitionEntry::Quotation(quotation_id)) => {
                self.activate_quotation(quotation_id);
                Ok(())
            }
            Some(DefinitionEntry::Primitive(primitive_id)) => {
                Err(ReturnFromRts::PrimitiveCalled(primitive_id))
            }
            None => Err(ReturnFromRts::DefinitionMissing(symbol_id)),
        }
    }

    fn execute_instruction(&mut self, instruction: Instruction) -> Result<(), ReturnFromRts> {
        match instruction {
            Instruction::Push(value) => {
                match value {
                    Value::StringLiteral(string_id) => {
                        self.data_stack.push(Data::ConstString(string_id));
                    }
                    Value::IntegerLiteral(i) => {
                        self.data_stack.push(Data::Integer(i as isize)); // XXX
                    }
                    Value::FloatLiteral(f) => {
                        self.data_stack.push(Data::Float(f));
                    }
                    Value::Quotation(quotation_id) => {
                        self.data_stack.push(Data::ConstQuotation(quotation_id));
                    }
                }
                Ok(())
            }
            Instruction::Call(symbol_id) => self.call_symbol(symbol_id),
            Instruction::Return => self.return_to_caller(),
        }
    }

    pub fn step(&mut self) -> Result<(), ReturnFromRts> {
        let InstructionPointer {
            instruction_offset,
            quotation_id,
        } = self.current_execution.ip;
        // advance instruction pointer
        self.current_execution.ip.instruction_offset += 1;

        let quotation = &self.quotations[quotation_id.0];
        match quotation.instruction_at(instruction_offset) {
            Some(instr) => self.execute_instruction(instr),
            None => {
                assert!(instruction_offset == quotation.len());
                // We are at the end of a quotation. pop the return_stack
                panic!();
            }
        }
    }

    pub fn run(&mut self) -> ReturnFromRts {
        loop {
            if let Err(return_from_rts) = self.step() {
                return return_from_rts;
            }
        }
    }
}
