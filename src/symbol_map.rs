use std::collections::HashMap;

pub struct SymbolMap {
    idx2string: Vec<String>,
    string2idx: HashMap<String, usize>,
}

impl SymbolMap {
    pub fn new() -> Self {
        Self {
            idx2string: Vec::new(),
            string2idx: HashMap::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.idx2string.len()
    }

    pub fn index(&self, idx: usize) -> &str {
        &self.idx2string[idx]
    }

    pub fn insert(&mut self, s: &str) -> usize {
        if let Some(&id) = self.string2idx.get(s) {
            debug_assert!(self.idx2string[id] == s);
            return id;
        }
        let new_id = self.idx2string.len();
        self.idx2string.push(s.to_string());
        self.string2idx.insert(s.to_string(), new_id);
        return new_id;
    }

    pub fn idx2string(self) -> Vec<String> {
        self.idx2string
    }
}
