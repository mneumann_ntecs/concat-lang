mod compiler;
pub(crate) mod rts;
pub(crate) mod symbol_map;
pub(crate) mod tokenizer;

use crate::compiler::{compile, InstructionSeq, PrimitiveId};
use crate::rts::{Data, ReturnFromRts, Rts};
use crate::tokenizer::tokenize;
use std::env::args;
use std::fs::read;

fn main() {
    let src_file = args().nth(1).unwrap();
    println!("Loading file: {}", src_file);
    let src_data = read(&src_file).unwrap();
    let src = std::str::from_utf8(&src_data).unwrap();

    let program = compile(&tokenize(src).unwrap());

    let mut rts = Rts::from_program(program);
    rts.register_global_primitive_from_str("SAY", PrimitiveId(0));
    rts.register_global_primitive_from_str("+", PrimitiveId(1));
    rts.register_global_primitive_from_str("CALL", PrimitiveId(2));
    rts.register_global_primitive_from_str("DEF", PrimitiveId(3));
    rts.register_global_primitive_from_str("EQ", PrimitiveId(4));
    rts.register_global_primitive_from_str("IF-THEN-ELSE", PrimitiveId(5));
    rts.register_global_primitive_from_str("NOT", PrimitiveId(6));
    rts.register_global_primitive_from_str("CMP", PrimitiveId(7));
    rts.register_global_primitive_from_str("DUP", PrimitiveId(8));
    rts.register_global_primitive_from_str("SWAP", PrimitiveId(9));
    rts.register_global_primitive_from_str("OR", PrimitiveId(10));
    rts.register_global_primitive_from_str("AND", PrimitiveId(11));
    rts.register_global_primitive_from_str("NEG", PrimitiveId(12));
    rts.register_global_primitive_from_str("DROP", PrimitiveId(13));
    rts.register_global_primitive_from_str("PRINT-STACK", PrimitiveId(14));
    rts.register_global_primitive_from_str("RESTART-IF-TRUE", PrimitiveId(15));
    rts.register_global_primitive_from_str("LOCAL", PrimitiveId(16));
    rts.register_global_primitive_from_str("LOCAL-ASSIGN", PrimitiveId(17));

    loop {
        match rts.run() {
            ReturnFromRts::ProgramExited => {
                println!("Program exited");
                break;
            }
            ReturnFromRts::DefinitionMissing(symbol_id) => {
                panic!(
                    "Definition missing: {:?} {}",
                    symbol_id,
                    rts.symbol_id_to_string(symbol_id)
                );
            }
            ReturnFromRts::PrimitiveCalled(primitive_id) => match primitive_id {
                PrimitiveId(0) => {
                    let item = rts.pop_data_stack();
                    match item {
                        Data::Integer(i) => println!("{}", i),
                        Data::Float(f) => println!("{}", f),
                        Data::ConstString(string_id) => {
                            println!("{}", rts.lookup_string_id(string_id));
                        }
                        _ => println!("{:?}", item),
                    }
                }
                PrimitiveId(1) => {
                    let op2 = rts.pop_data_stack();
                    let op1 = rts.pop_data_stack();
                    match (op1, op2) {
                        (Data::Integer(i1), Data::Integer(i2)) => {
                            let res = i1 + i2;
                            rts.push_data_stack(Data::Integer(res));
                        }
                        _ => {
                            panic!("invalid types");
                        }
                    }
                }
                PrimitiveId(2) => {
                    // CALL
                    match rts.pop_data_stack() {
                        Data::ConstQuotation(quotation_id) => {
                            rts.activate_quotation(quotation_id);
                        }
                        _ => {
                            panic!("invalid type for CALL (must be Quotation)");
                        }
                    }
                }
                PrimitiveId(3) => {
                    // DEF
                    let name = rts.pop_data_stack();
                    let quotation = rts.pop_data_stack();

                    match (quotation, name) {
                        (Data::ConstQuotation(quotation_id), Data::ConstString(string_id)) => {
                            let symbol_id = rts.symbol_id_for_string_id(string_id);
                            rts.register_global_quotation(symbol_id, quotation_id);
                        }
                        // XXX: handle DynString
                        _ => {
                            panic!("invalid types for DEF");
                        }
                    }
                }

                PrimitiveId(4) => {
                    // EQ
                    let op2 = rts.pop_data_stack();
                    let op1 = rts.pop_data_stack();
                    match (op1, op2) {
                        (Data::Integer(i1), Data::Integer(i2)) => {
                            rts.push_data_stack(Data::Bool(i1 == i2));
                        }
                        _ => {
                            panic!("invalid types");
                        }
                    }
                }
                PrimitiveId(5) => {
                    // IF-THEN-ELSE
                    let false_branch = rts.pop_data_stack();
                    let true_branch = rts.pop_data_stack();
                    let condition = rts.pop_data_stack();
                    match (condition, true_branch, false_branch) {
                        (Data::Bool(cond), Data::ConstQuotation(t), Data::ConstQuotation(f)) => {
                            if cond {
                                rts.activate_quotation(t);
                            } else {
                                rts.activate_quotation(f);
                            }
                        }
                        _ => {
                            panic!("invalid types");
                        }
                    }
                }
                PrimitiveId(6) => {
                    // NOT
                    let condition = rts.pop_data_stack();
                    match condition {
                        Data::Bool(cond) => {
                            rts.push_data_stack(Data::Bool(!cond));
                        }
                        _ => {
                            panic!("invalid types");
                        }
                    }
                }
                PrimitiveId(7) => {
                    // CMP
                    let op2 = rts.pop_data_stack();
                    let op1 = rts.pop_data_stack();
                    match (op1, op2) {
                        (Data::Integer(i1), Data::Integer(i2)) => {
                            let res = if i1 < i2 {
                                -1
                            } else if i1 > i2 {
                                1
                            } else {
                                0
                            };
                            rts.push_data_stack(Data::Integer(res));
                        }
                        (Data::Float(f1), Data::Float(f2)) => {
                            let res = if f1 < f2 {
                                -1
                            } else if f1 > f2 {
                                1
                            } else {
                                0
                            };
                            rts.push_data_stack(Data::Integer(res));
                        }
                        _ => {
                            panic!("invalid types");
                        }
                    }
                }

                PrimitiveId(8) => {
                    // DUP
                    let op = rts.pop_data_stack();
                    rts.push_data_stack(op.clone());
                    rts.push_data_stack(op);
                }
                PrimitiveId(9) => {
                    // SWAP
                    let op2 = rts.pop_data_stack();
                    let op1 = rts.pop_data_stack();
                    rts.push_data_stack(op2);
                    rts.push_data_stack(op1);
                }
                PrimitiveId(10) => {
                    // OR
                    let op2 = rts.pop_data_stack();
                    let op1 = rts.pop_data_stack();
                    match (op1, op2) {
                        (Data::Bool(b1), Data::Bool(b2)) => {
                            rts.push_data_stack(Data::Bool(b1 || b2));
                        }
                        _ => {
                            panic!("invalid types");
                        }
                    }
                }
                PrimitiveId(11) => {
                    // AND
                    let op2 = rts.pop_data_stack();
                    let op1 = rts.pop_data_stack();
                    match (op1, op2) {
                        (Data::Bool(b1), Data::Bool(b2)) => {
                            rts.push_data_stack(Data::Bool(b1 && b2));
                        }
                        _ => {
                            panic!("invalid types");
                        }
                    }
                }
                PrimitiveId(12) => {
                    // NEG
                    let op = rts.pop_data_stack();
                    match op {
                        Data::Integer(i1) => {
                            rts.push_data_stack(Data::Integer(-i1));
                        }
                        Data::Float(f1) => {
                            rts.push_data_stack(Data::Float(-f1));
                        }
                        _ => {
                            panic!("invalid types");
                        }
                    }
                }
                PrimitiveId(13) => {
                    // DROP
                    let _ = rts.pop_data_stack();
                }
                PrimitiveId(14) => {
                    // PRINT-STACK
                    println!("stack: {:?}", rts.data_stack());
                }

                PrimitiveId(15) => {
                    // RESTART-IF-TRUE. jumps to the beginning of the quotation
                    let condition = rts.pop_data_stack();
                    match condition {
                        Data::Bool(cond) => {
                            if cond {
                                rts.restart_current_quotation();
                            }
                        }
                        _ => {
                            println!("stack: {:?}", rts.data_stack());
                            panic!("invalid types");
                        }
                    }
                }

                PrimitiveId(16) => {
                    // LOCAL: defines a local variable
                    let name = rts.pop_data_stack();
                    //let value = rts.pop_data_stack();

                    match name {
                        Data::ConstString(string_id) => {
                            let symbol_id = rts.symbol_id_for_string_id(string_id);
                            rts.define_local_variable(symbol_id);
                        }
                        _ => panic!("invalid types for LOCAL"),
                    }
                }
                PrimitiveId(17) => {
                    // LOCAL-ASSIGN: sets a local variable
                    let name = rts.pop_data_stack();
                    let value = rts.pop_data_stack();

                    match name {
                        Data::ConstString(string_id) => {
                            let symbol_id = rts.symbol_id_for_string_id(string_id);
                            rts.set_local_variable(symbol_id, value);
                        }
                        _ => panic!("invalid types for LOCAL"),
                    }
                }

                _ => {
                    panic!("Undefined primitive: {:?}", primitive_id);
                }
            },
        }
    }
}
