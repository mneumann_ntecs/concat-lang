#[derive(Debug)]
pub enum Token<'a> {
    BegQuote,
    EndQuote,
    Atom(&'a str),
    StringLiteral(String),
    IntegerLiteral(usize),
    FloatLiteral(f64),
    Comment,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    InvalidFloatLiteral,
    InvalidIntegerLiteral,
    UnterminatedStringLiteral,
    EndOfString,
}

pub fn tokenize<'a>(input: &'a str) -> Result<Vec<Token<'a>>, Error> {
    let mut tokens = Vec::new();
    let mut s = input;
    loop {
        match next_token(s) {
            Ok((next, token)) => {
                s = next;
                tokens.push(token);
            }
            Err(Error::EndOfString) => {
                return Ok(tokens);
            }
            Err(err) => {
                return Err(err);
            }
        }
    }
}
#[test]
fn test_tokenize() {
    use assert_matches::assert_matches;
    assert_matches!(
        tokenize("[ a b \n] # blah blah blah \n a")
            .as_ref()
            .map({ |s| &s[..] }),
        Ok(&[
            Token::BegQuote,
            Token::Atom("a"),
            Token::Atom("b"),
            Token::EndQuote,
            Token::Comment,
            Token::Atom("a"),
        ])
    );
}

fn next_token<'a>(input: &'a str) -> Result<(&'a str, Token<'a>), Error> {
    let start_of_token = input.trim_start();
    let mut iter = start_of_token.chars();
    match iter.next() {
        Some('#') => {
            loop {
                match iter.next() {
                    Some('\n') | None => {
                        break;
                    }
                    _ => {}
                }
            }
            return Ok((iter.as_str(), Token::Comment));
        }
        Some('"') => {
            let mut s = String::new();
            loop {
                match iter.next() {
                    Some('"') => {
                        // either end of string or escaped ""
                        match iter.next() {
                            Some('"') => {
                                // escaped '"'
                                s.push('"');
                            }
                            Some(c) if char::is_whitespace(c) => {
                                return Ok((iter.as_str(), Token::StringLiteral(s)));
                            }
                            None => {
                                return Ok((iter.as_str(), Token::StringLiteral(s)));
                            }
                            _ => {
                                return Err(Error::UnterminatedStringLiteral);
                            }
                        }
                    }
                    Some(c) => {
                        s.push(c);
                    }
                    None => {
                        return Err(Error::UnterminatedStringLiteral);
                    }
                }
            }
        }
        Some(c) if char::is_digit(c, 10) => {
            // this should be a digit
            let (token, remainder) = match start_of_token.find(char::is_whitespace) {
                None => (start_of_token, ""),
                Some(pos) => start_of_token.split_at(pos),
            };

            if token.contains(".") {
                match token.parse::<f64>() {
                    Ok(lit) => {
                        return Ok((remainder, Token::FloatLiteral(lit)));
                    }
                    Err(_) => {
                        return Err(Error::InvalidFloatLiteral);
                    }
                }
            } else {
                match token.parse::<usize>() {
                    Ok(lit) => {
                        return Ok((remainder, Token::IntegerLiteral(lit)));
                    }
                    Err(_) => {
                        return Err(Error::InvalidIntegerLiteral);
                    }
                }
            }
        }
        Some(_) => {
            let (token, remainder) = match start_of_token.find(char::is_whitespace) {
                None => (start_of_token, ""),
                Some(pos) => start_of_token.split_at(pos),
            };
            if token == "[" {
                return Ok((remainder, Token::BegQuote));
            } else if token == "]" {
                return Ok((remainder, Token::EndQuote));
            } else {
                return Ok((remainder, Token::Atom(token)));
            }
        }

        None => Err(Error::EndOfString),
    }
}

#[test]
fn test_next_token() {
    use assert_matches::assert_matches;
    assert_matches!(next_token("[ a b ]"), Ok((" a b ]", Token::BegQuote)));
    assert_matches!(next_token(" a b ]"), Ok((" b ]", Token::Atom("a"))));
    assert_matches!(next_token(" b ]"), Ok((" ]", Token::Atom("b"))));
    assert_matches!(next_token(" ]"), Ok(("", Token::EndQuote)));
    assert_matches!(next_token("1234.5"), Ok(("", Token::FloatLiteral(_))));
    assert_matches!(next_token("1234"), Ok(("", Token::IntegerLiteral(1234))));
    assert_matches!(next_token("\"hallo\""), Ok(("", Token::StringLiteral(ref s))) if s == "hallo");
    assert_matches!(next_token("\"hallo\"\"leute\""), Ok(("", Token::StringLiteral(ref s))) if s == "hallo\"leute");
    // next_token on a string literal needs lookahead, i.e. consumes 1 character
    assert_matches!(next_token("\"hallo\" 123"), Ok(("123", Token::StringLiteral(ref s))) if s == "hallo");
}
