use crate::symbol_map::SymbolMap;
use crate::tokenizer::Token;

// When compiling, we extract the strings and quotations and assign
// each of them a number.
#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct StringId(pub usize);
#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub struct QuotationId(pub usize); // quotations are anonymous "functions".
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash)]
pub struct SymbolId(pub usize); // named functions (Forth calls them "words").
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash)]
pub struct PrimitiveId(pub usize);

#[derive(Debug, Copy, Clone)]
pub enum Value {
    StringLiteral(StringId),
    IntegerLiteral(usize),
    FloatLiteral(f64),
    Quotation(QuotationId),
}

#[derive(Debug, Copy, Clone)]
pub enum Instruction {
    /// Push a value on the data stack.
    Push(Value),
    /// Call a subroutine.
    Call(SymbolId),
    /// Return from a subroutine call.
    Return,
}

// XXX: ideally we would keep a reference to the token tree here for debugging
// purposes.
#[derive(Debug)]
pub struct InstructionSeq {
    instructions: Vec<Instruction>,
}

impl InstructionSeq {
    pub fn new() -> Self {
        Self {
            instructions: Vec::new(),
        }
    }

    pub fn push(&mut self, instruction: Instruction) {
        self.instructions.push(instruction);
    }

    pub fn len(&self) -> usize {
        self.instructions.len()
    }

    pub fn instruction_at(&self, offset: usize) -> Option<Instruction> {
        self.instructions.as_slice().get(offset).cloned()
    }
}

/// This is the representation of a compiled program.
/// It does not contain primitives required for execution.
pub struct Program {
    /// Contains the constant string literals
    pub string_constants: Vec<String>,

    /// Quotations are anonymous functions. We refer to each using a QuotationId.
    pub quotations: Vec<InstructionSeq>,

    /// We compile atoms to ids.
    pub symbols: SymbolMap,

    /// This is where we start execution when run this program.
    pub entry_point: QuotationId,
}

pub fn compile(tokens: &[Token]) -> Program {
    let mut strings = SymbolMap::new();
    let mut symbols = SymbolMap::new();
    let mut quotations: Vec<InstructionSeq> = Vec::new();
    let mut iseq_stack = vec![InstructionSeq::new()];

    for token in tokens.iter() {
        match token {
            Token::BegQuote => {
                iseq_stack.push(InstructionSeq::new());
            }
            Token::EndQuote => {
                let mut quotation = iseq_stack.pop().unwrap();
                quotation.push(Instruction::Return);

                // XXX: We could compare if we already found a quotation with the same
                // "content".
                let id = quotations.len();
                quotations.push(quotation);
                iseq_stack
                    .last_mut()
                    .unwrap()
                    .push(Instruction::Push(Value::Quotation(QuotationId(id))));
            }
            Token::Atom(ref atom) => {
                let id = symbols.insert(atom);
                iseq_stack
                    .last_mut()
                    .unwrap()
                    .push(Instruction::Call(SymbolId(id)));
            }
            Token::StringLiteral(ref s) => {
                let id = strings.insert(s);
                iseq_stack
                    .last_mut()
                    .unwrap()
                    .push(Instruction::Push(Value::StringLiteral(StringId(id))));
            }
            &Token::IntegerLiteral(i) => {
                iseq_stack
                    .last_mut()
                    .unwrap()
                    .push(Instruction::Push(Value::IntegerLiteral(i)));
            }
            &Token::FloatLiteral(f) => {
                iseq_stack
                    .last_mut()
                    .unwrap()
                    .push(Instruction::Push(Value::FloatLiteral(f)));
            }
            &Token::Comment => {
                // ignore
            }
        }
    }

    let mut entry_point = iseq_stack.pop().unwrap();
    assert!(iseq_stack.is_empty());
    entry_point.push(Instruction::Return);

    let entry_point_id = quotations.len();
    quotations.push(entry_point);

    Program {
        string_constants: strings.idx2string(),
        symbols,
        quotations,
        entry_point: QuotationId(entry_point_id),
    }
}

#[test]
fn test_compile_program1() {
    use crate::tokenizer::tokenize;
    use assert_matches::assert_matches;
    {
        let p = compile(&tokenize("\"hallo\" say").unwrap());
        assert_eq!(1, p.string_constants.len());
        assert_eq!("hallo", p.string_constants[0]);
        assert_eq!(1, p.symbols.len());
        assert_eq!("say", p.symbols.index(0));
        assert_eq!(1, p.quotations.len());
        assert_matches!(
            p.quotations[0].instructions.as_slice(),
            &[
                Instruction::Push(Value::StringLiteral(StringId(0))),
                Instruction::Call(SymbolId(0)),
                Instruction::Return,
            ]
        );
        assert_eq!(QuotationId(0), p.entry_point);
    }
}

#[test]
fn test_compile_program2() {
    use crate::tokenizer::tokenize;
    use assert_matches::assert_matches;
    {
        let p = compile(&tokenize("[ 1 2 3 + * ] 5 loop ").unwrap());
        assert_eq!(0, p.string_constants.len());
        assert_eq!(3, p.symbols.len());
        assert_eq!("+", p.symbols.index(0));
        assert_eq!("*", p.symbols.index(1));
        assert_eq!("loop", p.symbols.index(2));
        assert_eq!(2, p.quotations.len());
        assert_matches!(
            p.quotations[0].instructions.as_slice(),
            &[
                Instruction::Push(Value::IntegerLiteral(1)),
                Instruction::Push(Value::IntegerLiteral(2)),
                Instruction::Push(Value::IntegerLiteral(3)),
                Instruction::Call(SymbolId(0)),
                Instruction::Call(SymbolId(1)),
                Instruction::Return,
            ]
        );
        assert_matches!(
            p.quotations[1].instructions.as_slice(),
            &[
                Instruction::Push(Value::Quotation(QuotationId(0))),
                Instruction::Push(Value::IntegerLiteral(5)),
                Instruction::Call(SymbolId(2)),
                Instruction::Return,
            ]
        );

        assert_eq!(QuotationId(1), p.entry_point);
    }
}
